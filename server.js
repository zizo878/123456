var express=require('express')
var app=express()
app.use(express.static('public'))

app.get('/get',(req,res)=>{
    res.send('hello,get');
})
app.get('/post',(req,res)=>{
    res.send('hello,post');
})
app.get('/xml',(req,res)=>{
    res.send('ok');
})
app.get('/xml',(req,res)=>{
    res.setHeader('Content-Type','text/xml')
    res.send('<?xml version="1.0" encoding="UTF-8"?><book><name>红楼梦</name><about>曹雪芹</about></book>');
})
app.get('/json',(req,res)=>{
    res.send('ok');
})
app.get('/json',(req,res)=>{
    var obj={
        "name":"adlerian",
    "age":"16",
    "anhao":{
        "book":"人类简史",
        "ox":"rose",
        "yingy":"《I love host》"
    }
    }
    res.send(obj);
})
app.get('/cors',(req,res)=>{
    res.send('服务器发送成功');
})
app.listen(3000,() =>{
    console.log('服务器自动。。。');
}) 